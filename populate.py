# -*- coding: utf-8 -*-
from mapper.server.models import *

if __name__ == '__main__':
    setup_all()
    create_all()
    for x in range(10):
        location = Location()
        location.name = "Loc%d" % x
        location.latitude = 1.1111
        location.longitude = 1.23
        session.commit()
    user = User()
    user.first_name = 'gio'
    user.last_name = 'AOUN'
    user.username = 'gio'
    user.password = 'test'
    session.commit()
