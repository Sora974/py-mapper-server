#!/user/bin/python env
import logging
import sys
from elixir import setup_all, create_all
from mapper.server.tornadio.server import application

if __name__ == "__main__":
    setup_all()
    create_all()
    logging.debug('DB set up !')
    sys.exit(application())
