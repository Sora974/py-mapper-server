# -*- coding: utf-8 -*-
from elixir import setup_all, create_all
from mapper.server.models import User

setup_all()
create_all()


def authenticate(username, password):
    user = User.get_by(username=username)
    if user and password and user.password == password:
        return user
