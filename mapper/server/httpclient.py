#coding=utf-8
import json
import urllib2
import logging


log = logging.getLogger(__name__)


class HttpClient(object):

    def __init__(self, url=None, format=None):
        if not url:
            self.url = (
                "http://maps.googleapis.com/maps/api/geocode/"
                "%(format)s?address=%(address)s&sensor=false"
            )
        else:
            self.url = url
        self.format = format if format else "json"

    def get_coordinate(self, location, format=None):
        if format:
            self.format = format
        address = location.address.decode(
            'iso-8859-1').encode('utf-8').replace(' ', '+')
        url = self.url % {"address": address, "format": self.format}
        log.info(url)
        results = json.loads(" ".join(urllib2.urlopen(url).readlines()))

        if len(results["results"]):
            result = results["results"][0]
            log.info(result)
            location.address = result["formatted_address"]
            location.latitude = result["geometry"]["location"]["lat"]
            location.longitude = result["geometry"]["location"]["lng"]
            log.info(location.to_dict())
