from elixir import *
from mapper.server.auth import authenticate
from mapper.server.models import Location
from tornadio2.conn import event
import logging
import tornadio2
from mapper.server.httpclient import HttpClient


log = logging.getLogger(__name__)


class IOConnection(tornadio2.conn.SocketConnection):
    # Class level variable
    participants = set()

    def __init__(self, *args, **kwargs):
        super(IOConnection, self).__init__(*args, **kwargs)
        self.authenticated = False
        self.user = None  # our authenticated user
        self.http_client = HttpClient()

    @event('authenticate')
    def authentication(self, username, password):
        """ Login pour le socket """
        log.info("Data received = %s | %s" % (username, password))
        user = authenticate(username=username, password=password)
        log.info(user.__dict__)
        log.info("authenticating ...")
        if user is not None:
            self.authenticated = True
            self.user = user
            print(user.to_dict())
            self.emit(
                'authenticate',
                {
                    "authenticated": True,
                    "msg": "Authentication successful !",
                    "user": user.to_dict()
                }
            )
        else:
            self.emit(
                'authenticate',
                {
                    "authenticated": False,
                    "msg": "Wrong credentials, authentication failed"
                }
            )
        log.info(self.authenticated)
        log.info(user)

    @event("get_user_loc")
    def getUserLocations(self, user_id):
        locations = Location.query.filter_by(user_id=user_id)
        json_locations = []
        for location in locations:
            json_locations.append(location.to_dict())
        self.emit('get_user_loc', json_locations)

    @event("save_location")
    def saveLocation(self, loc):
        print "LOCATION JSON = ", loc
        if "id" not in loc or loc["id"] is None:   # add new location
            location = Location()
        else:                       # update existing location
            location = Location.get(loc["id"])
        location.update(loc)  # update location fields

        print "LOCATION DICT = ", location.to_dict()
        # TODO = call Google Maps WS to get coordinates
        self.http_client.get_coordinate(location)

        session.commit()

        self.emit("save_location", location.to_dict())

    @event("delete_location")
    def onDeleteLocation(self, loc):
        location = Location.get(loc["id"])
        Location.delete(location)
        session.commit()

        self.emit("delete_location", location.to_dict())

    @event
    def ping(self, msg):
        self.emit("pingback")

    def on_open(self, info):
        self.participants.add(self)
        log.info("Connected !")
        self.emit('toto', {"msg": "Connected!"})

    def on_message(self, message):
        # Pong message back
        for p in self.participants:
            p.send(message)

    def on_close(self):
        self.participants.remove(self)

    def _check_token(self, token):
        return True if self.api_key == token else False
