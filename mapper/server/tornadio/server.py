# -*- coding: utf-8 -*-
from tornadio2 import server
from tornado.options import parse_command_line
from .. import settings
import sys
import os
import tornadio2
import tornado
import logging
from mapper.server.tornadio.handler import IOConnection


log = logging.getLogger(__name__)


class IndexHandler(tornado.web.RequestHandler):
    """Index Page handler : TESTING ONLY"""
    def get(self):
        template = os.path.join(settings.DOCUMENT_ROOT, 'resources', 'io.html')
        self.render(template)


class SocketIOHandler(tornado.web.RequestHandler):
    def get(self):
        self.render(settings.RESOURCES_ROOT + '/socket.io.js')


class WebSocketFileHandler(tornado.web.RequestHandler):
    def get(self):
        # Obviously, you want this on CDN, but for sake of
        # example this approach will work.
        self.set_header('Content-Type', 'application/x-shockwave-flash')
        with open(settings.RESOURCES_ROOT + '/WebSocketMain.swf', 'rb') as f:
            self.write(f.read())
            self.finish()


def application(argv=None):
    print "start"
    parse_command_line()
    if not argv:
        argv = sys.argv

    # Create the server
    SocketRouter = tornadio2.router.TornadioRouter(
        IOConnection,
        dict(websocket_check=True)
    )
    # configure the Tornado application
    # currently only allow one command-line argument, the port to run on.
    port = int(argv[1]) if (len(argv) > 1) else settings.SOCKET_IO_PORT

    application = tornado.web.Application(
        SocketRouter.apply_routes(
            [
                (r"/", IndexHandler),
                (r"/socket.io.js", SocketIOHandler),
                (r"/resources/(.*)",
                    tornado.web.StaticFileHandler,
                    {"path": settings.RESOURCES_ROOT}),
                (r"/WebSocketMain.swf", WebSocketFileHandler)
            ]),
        flash_policy_port=8043,
        flash_policy_file=os.path.join(
            settings.DOCUMENT_ROOT, '/flashpolicy.xml'),
        socket_io_port=port,
    )
    log.info(
        "LocationMapper Server running on %s:%d",
        settings.SOCKET_IO_REMOTE_ADDR,
        port
    )
    return server.SocketServer(application)
