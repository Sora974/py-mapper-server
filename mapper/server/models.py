# -*- coding: utf-8 -*-
from elixir import *


metadata.bind = "sqlite:///locmapper.sqlite"
#metadata.bind = create_engine("mysql://root@localhost/mapper", echo_pool=True)
metadata.bind.echo = True
setup_all(True)


class User(Entity):
    """ This is our User entity """
    username = Field(Unicode(50))
    password = Field(Unicode(50))
    first_name = Field(Unicode(50))
    last_name = Field(Unicode(50))
    locations = OneToMany('Location')

    using_options(tablename='locmap_users')

    def to_dict(self):
        return {
            "id": self.id,
            "username": self.username,
            "first_name": self.first_name,
            "last_name": self.last_name
        }

    def __repr__(self):
        return self.username


class Location(Entity):
    name = Field(Unicode(100))
    address = Field(Unicode(255))
    latitude = Field(Float(100))
    longitude = Field(Float(100))
    user = ManyToOne('User')

    using_options(tablename='locmap_locations')

    def to_dict(self):
        return {
            "id": self.id,
            "name": self.name,
            "address": self.address,
            "latitude": self.latitude,
            "longitude": self.longitude,
            "user_id": self.user_id
        }

    def __repr_(self):
        return self.name

    def update(self, data):
        for key in data:
            if hasattr(self, key):
                setattr(self, key, data[key])

    def update_entries(self, data):
        self.__dict__.update(data)


class Struct:
    def __init__(self, **entries):
        self.__dict__.update(entries)
