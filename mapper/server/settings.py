#coding=utf-8

from os.path import abspath, dirname, join
import mapper


DEBUG = True

SOCKET_IO_PORT = 8080

SOCKET_IO_REMOTE_ADDR = "127.0.0.1"

TORNADIO_LOGGING_LEVEL = "debug" if DEBUG else "info"

# "/Users/Sora/workspace/python/pyMapperServer"
DOCUMENT_ROOT = dirname(abspath(mapper.__file__))

RESOURCES_ROOT = join(DOCUMENT_ROOT, "resources")
