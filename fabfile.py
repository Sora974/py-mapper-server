from mapper.server import settings as mapper_settings
from fabric.api import local, env
from fabric.contrib import django
import os
import sys


root_path = os.path.dirname(os.path.abspath(__file__))
# needed to push youfood.settings onto the path.
sys.path.append(root_path)

# default env configuration
env.python = 'python'
env.project_name = 'pyMapperServer'
# default to current working directory
env.project_path = os.path.dirname(__file__)
env.hosts = ['localhost']
env.apache = 'httpd'
# django integration for access to settings, etc.
django.project(env.project_name)


def shell():
    local("{python} manage.py shell".format(python=env.python), capture=False)


def runservers():
    print os.path.abspath('.')
    print sys.path
    raw_input()
    local("{python} mapper\\server\\tornadio\\server.py".format(
        python=env.python))
    tornadio()


def tornadio(ip="127.0.0.1", port=None, logging=None):
    if not port:
        port = mapper_settings.SOCKET_IO_PORT
    if not logging:
        logging = mapper_settings.TORNADIO_LOGGING_LEVEL or 'ERROR'
    local("start {python} tornadio.py {port} --logging={log_level}".format(
        python=env.python,
        port=port,
        log_level=logging)
    )
